import styled from 'styled-components';
import { Link } from "react-router-dom";

// * exports: 
// NavigationContainer, 
// NavigationLogoContainer, 
// NavigationLogoTitle, 
// NavigationLinksContainer, 
// NavigationLink, 
// RootBackground, 
// OutletContainer, 
// NavigationLogoContainer

export const NavigationContainer = styled.div`
    /* height: 70px; */
    width: 100%;
    display: flex;
    padding: 20px 40px;
    justify-content: space-between;
    // margin-bottom: 25px;
    background-color: rgb(40, 0, 31);  
    position: fixed;
    top: 0px;
    z-index: 999;
    `;

export const NavigationLogoContainer = styled(Link)`
    display: grid;
    grid-template-columns: auto auto;
    height: 100%;
    width: auto;
    // padding: 25px;
`;

export const NavigationLogoTitle = styled.span`
    color: aliceblue;
    `
export const NavigationLinksContainer = styled.div`
    width: 50%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    `;

export const NavigationLink = styled(Link)`
  
    color: aliceblue;
    padding: 10px 15px;
    cursor: pointer;
  `;

export const RootBackground = styled.div`
    background-color: rgb(40, 0, 31);
    min-height: 100vh;
    height: 100%;
    `
export const OutletContainer = styled.div`
    padding-top: 100px;

`