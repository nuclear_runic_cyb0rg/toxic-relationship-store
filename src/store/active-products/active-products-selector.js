import { createSelector } from "reselect"


export const selectActiveProductsReducer = state => state.activeProducts 

export const selectActiveProducts = createSelector(
    [selectActiveProductsReducer],
    activeProducts => activeProducts.activeProductList
)

export const selectIsCartOpen = createSelector(
    [selectActiveProductsReducer],
    activeProducts => activeProducts.isCartOpen
)