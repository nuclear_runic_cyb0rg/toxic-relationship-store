import { Route, Routes } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { fetchCategoriesAsync } from "../../store/categories/categories-action.js";
import MarketplacePreview from "../../components/marketplace/marketplace-preview.component/marketplace-preview.component";
import CategoryPreview from "../../components/marketplace/marketplace-category-preview.component/marketplace-category-preview.component";

const Marketplace = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchData = async () => {
      const categories = await fetchCategoriesAsync();
      dispatch(categories)
    }
    fetchData()
  },
   [dispatch]);
  return (
    <Routes>
      <Route index element={<MarketplacePreview />} />
      <Route path="/:category" element={<CategoryPreview />} />
    </Routes>
  );
};


export default Marketplace;
