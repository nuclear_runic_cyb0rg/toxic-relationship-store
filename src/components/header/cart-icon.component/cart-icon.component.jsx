import React from "react";
import CartSVG from "../../../assets/cart.png";
import CartDropdown from "../cart-dropdown.component/cart-dropdown.component";
import { useSelector } from "react-redux"
import { selectActiveProducts, selectIsCartOpen } from "../../../store/active-products/active-products-selector";
import { useDispatch } from "react-redux"
import { setCartOpen } from "../../../store/active-products/active-products-action"
import {
    CartIconContainer,
    CartIconAmount
} from "./cart-icon.styles";

const CartIcon = () => {
    const dispatch = useDispatch()
    const activeProductList = useSelector(selectActiveProducts);
    const isCartOpen = useSelector(selectIsCartOpen)
    
    const iconOnClickButtonHandler = () => {
        dispatch(setCartOpen(!isCartOpen))
    };

    let newActiveProductCount = activeProductList.reduce((prevValue, item) => prevValue + item.amount, 0)

    return (
        <>
            <CartIconContainer onClick={iconOnClickButtonHandler}>
                <img src={CartSVG} alt="Cart icon :)" />
                <CartIconAmount>
                    <b>{newActiveProductCount}</b>
                </CartIconAmount>
            </CartIconContainer>
            {isCartOpen && <CartDropdown />}
        </>
    );
};

export default CartIcon;
