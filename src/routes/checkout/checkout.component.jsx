import {
    CheckoutContainer,
    CheckoutItem,
    CheckoutItemImage,
    CheckoutControlPanel,
    ChangeAmountButton,
    TotalPrice
} from "./checkout.styles.jsx"
import { selectActiveProducts } from "../../store/active-products/active-products-selector"
import { changeItemAmount } from "../../store/active-products/active-products-action"
import { useSelector, useDispatch } from "react-redux"

const Checkout = () => {
    const activeProductList = useSelector(selectActiveProducts)
    const dispatch = useDispatch()

    const countTotalSum = () => {
        const newList = [...activeProductList]
        return newList.reduce((previousValue, product) => {
            const productTotal = product.amount * product.price
            return previousValue + productTotal
        }, 0)
    }

    // types: increase, decrease

    if (!activeProductList.length) {
        return (<CheckoutContainer>No products added</CheckoutContainer>)
    }
    else {
        return (<CheckoutContainer>
            {activeProductList.map((item) => {
                const { amount, name, price, imageUrl } = item
                const buttonHandlers = {
                    "increase": () => dispatch(changeItemAmount(item, "increase")),
                    "decrease": () => dispatch(changeItemAmount(item, "decrease"))
                }

                return (
                    <CheckoutItem key={item.id}>

                        <div><CheckoutItemImage src={imageUrl} alt={name} /> </div>

                        <CheckoutControlPanel>
                            <ChangeAmountButton onClick={buttonHandlers['decrease']}>&#10094;</ChangeAmountButton>
                            {name} x {amount}
                            <ChangeAmountButton onClick={buttonHandlers['increase']}>&#10095;</ChangeAmountButton>
                        </CheckoutControlPanel>

                        <TotalPrice>{"$" + (price * amount)}</TotalPrice>
                    </CheckoutItem>
                )
            })}
            <div>
                {"TOTAL: $" + countTotalSum()}
            </div>
        </CheckoutContainer>)
    }
}

export default Checkout