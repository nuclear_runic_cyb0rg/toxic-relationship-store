import { Link } from "react-router-dom";
import styled from "styled-components";

//* exports: 
// MarketplaceContainer, 
// CategoryContainer, 
// CategoryTitle, 
// CategoryProductsContainer

export const MarketplaceContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1px;
    background: linear-gradient(to top, #ff0256, #b30050, #730045, #330029, #510156);
    border-top: 4px dotted #ffd000;
`
export const CategoryContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: center;
    border-radius: 5px;
    height: 100%;
    box-sizing: border-box;
    padding-bottom: 10px;
    
`

export const CategoryTitle = styled(Link)`
    font-size: 1.5rem;
    font-weight: bold;
    text-transform: capitalize;
    color: white;
    text-transform: uppercase;
    padding: 10px;
`

export const CategoryProductsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    column-gap: 10px;
    width: 80%;
    
    /* column-gap: 40px; */
`