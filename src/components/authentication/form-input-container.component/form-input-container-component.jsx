import {
    GroupContainer,
    FormInputLabel,
    FormInputContainer
}
    from "./form-input-container-component.styles.jsx"

const FormInput = ({ label, value, ...otherProps }) => {
    return (
        <GroupContainer>
            <FormInputContainer value={value} {...otherProps} />
            {label &&
                <FormInputLabel shrink={value && value.length} htmlFor={otherProps.name}>
                    {label}
                </FormInputLabel>
            }
        </GroupContainer>
    )
}

export default FormInput