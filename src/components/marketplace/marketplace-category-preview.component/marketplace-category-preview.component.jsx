import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectCategories } from "../../../store/categories/categories-selector";

import ProductCard from "../../common/product-card.component/product-card.component";

import {
  CategoryPreviewContainer,
  CategoryPreviewTitle,
  CategoryPreviewProductsContainer
} from "./marketplace-category-preview.styles"

const categoryEmojis = {
  hats: "🧢",
  sneakers: "👟",
  jackets: "👔",
  mens: "👨",
  womens: "👩",
}

const CategoryPreview = () => {

  // In this case, category name
  // marketplace/<category_name>
  const { category } = useParams();

  const categoriesMap = useSelector(selectCategories);

  const categoryProducts = categoriesMap[category];

  return (
    <CategoryPreviewContainer>
      <CategoryPreviewTitle>
        {`${categoryEmojis[category]} Toxic ${category}`}
      </CategoryPreviewTitle>
      <CategoryPreviewProductsContainer>
        {
          categoryProducts &&
          categoryProducts.map(product => (
            <ProductCard key={product.id} product={product} />
          ))
        }
      </CategoryPreviewProductsContainer>
    </CategoryPreviewContainer>
  );
};

export default CategoryPreview;