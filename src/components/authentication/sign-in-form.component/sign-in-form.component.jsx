
import Button from "../../common/button.component/button.component.jsx"
import FormInput from "../form-input-container.component/form-input-container-component.jsx"
import { useState } from "react"
import { checkUserEmail, logGoogleUserWithPopup } from "../../../utils/firebase/firebase.utils.js"

import {
    LoginForm, 
    ButtonContainer
} from "./sign-in-form.component.styles.jsx"
const defaultSubmitForm = {
    email: "",
    password: ""
}

const SignIn = () => {
    const [loginData, setLoginData] = useState(defaultSubmitForm)
    const { email, password } = loginData
    const resetSignUpForm = () => {
        setLoginData(defaultSubmitForm)
    }

    const onChangeHandler = (event) => {
        const { name, value } = event.target
        setLoginData({ ...loginData, [name]: value })

    }

    const onSubmitHandler = async (event) => {
        event.preventDefault()
        await checkUserEmail(email, password)


        resetSignUpForm()
    }

    // const onGoogleAuthHandler = async (event) => {
    //     const {user} = await logGoogleUserWithPopup()
    //     setCurrentUser(user)
    // }

    return (
        <>
            <LoginForm onSubmit={onSubmitHandler}>
                <h3>Welcome Back!</h3>
                <FormInput
                    label="Email"
                    type="email"
                    name="email"
                    value={email}
                    onChange={onChangeHandler}
                    required={true} />
                <FormInput
                    label="Password"
                    type="password"
                    name="password"
                    onChange={onChangeHandler}
                    value={password}
                    required={true} />
                <ButtonContainer>
                    <Button
                        text="Login"
                        buttonType={'default'}
                        type='submit'
                    />
                    <Button
                        text="Google"
                        type="button"
                        buttonType={'google'}
                        onClick={logGoogleUserWithPopup} />

                </ButtonContainer>
            </LoginForm>
        </>
    )
}

export default SignIn