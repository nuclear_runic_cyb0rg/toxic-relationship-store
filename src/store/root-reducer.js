import { combineReducers } from 'redux';
import { userReducer } from './user/user-reducer';
import { categoriesReducer } from './categories/categories-reducer';
import { activeProductsReducer } from './active-products/active-products-reducer';

export const rootReducer = combineReducers({
    user: userReducer,
    categories: categoriesReducer,
    activeProducts: activeProductsReducer
})