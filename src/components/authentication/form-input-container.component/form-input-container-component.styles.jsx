import styled, { css } from "styled-components";

// $sub-color: rgb(185, 184, 184);
// $main-color: rgb(255, 255, 255);
const subColor = "rgb(185, 184, 184)";
const mainColor = "rgb(255, 255, 255)";

const shrinkLabelStyles = css`
  top: -16px;
  font-size: 12px;
  color: ${mainColor};
`;

//* exports: 
// GroupContainer, 
// PasswordInput, 
// FormInputLabel, 
// FormInputContainer

export const GroupContainer = styled.div`
  position: relative;
  margin: 30px 2px ;
`

export const PasswordInput = styled.input`
    letter-spacing: 0.3em;
  `

export const FormInputLabel = styled.label`
  color: ${subColor};
  font-size: 16px;
  font-weight: normal;
  position: absolute;
  pointer-events: none;
  left: 5px;
  top: 10px;
  transition: 300ms ease all;

  ${(props) => props.shrink && shrinkLabelStyles}
`
export const FormInputContainer = styled.input`
    background: none;
    background-color: rgb(52, 0, 44);
    color: ${subColor};
    font-size: 16px;
    padding: 10px 10px 10px 5px;
    display: block;
    width: 100%;
    border: none;
    border-radius: 7px;
    border-bottom: 1px solid ${subColor};

    &:focus {
      outline: none;
    }

    &:focus ~ ${FormInputLabel} {
      ${shrinkLabelStyles}
  }
`
