import { 
    CartItemContainer,
CartItemDeleteButton,
CartItemTextContainer,
CartItemImage,
CartItemName,
CartItemPrice

 } from "./cart-item.component.styles"
/**
 *   {
    "id": 2,
    "name": "Blue Beanie",
    "imageUrl": "https://i.ibb.co/ypkgK0X/blue-beanie.png",
    "price": 18
  }
 */

const CartItem = ({ product }) => {
    // console.log(id)
    console.log(product)
    // console.log(imageUrl)
    // console.log(price)
    const { name, id, imageUrl, price, amount } = product
    return (
        <CartItemContainer key={id} >
            <CartItemImage src={imageUrl} alt={name} />
            <CartItemTextContainer>
                <CartItemName>
                    {name} x {amount}
                </CartItemName>
                <CartItemPrice>
                    {price + " $"}
                </CartItemPrice>
            </CartItemTextContainer>
            <CartItemDeleteButton>X</CartItemDeleteButton>
        </CartItemContainer>
    )
}

export default CartItem