import styled from "styled-components";

//* exports:
// CategoryPreviewContainer,
// CategoryPreviewTitle,
// CategoryPreviewProductsContainer

export const CategoryPreviewContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: -20px; 
  background: linear-gradient(to bottom, rgb(40, 0, 31), #5e0045);
  height: 100vh;
`

export const CategoryPreviewTitle = styled.span`
  text-align: center;
  width: 100%;
  margin-left: auto;
  font-weight: bold;
  text-transform: capitalize;
  font-size: 2em;
  color: rgb(254, 224, 249);
`

export const CategoryPreviewProductsContainer = styled.div`
  border: black solid 2px;
  background: linear-gradient(to right, #f4c4c4, #ffbbe0);
  border-radius: 10px;
  margin: 0px 30px;
  display: grid;
  grid-template-columns: repeat(5, auto);
  grid-gap: 1em;
  padding: 1em 3em;
`