import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const colorChange = keyframes`
  0% {
    border-top-color: #3498db;
  }
  33% {
    border-top-color: #e74c3c;
  }
  66% {
    border-top-color: #f1c40f;
  }
  100% {
    border-top-color: #2ecc71;
  }
`;

export const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
`;

export const Spinner = styled.div`
  width: 50px;
  height: 50px;
  border: 5px solid rgba(0, 0, 0, 0.1);
  border-radius: 50%;
  border-top: 5px solid #3498db;
  animation: ${spin} 1s linear infinite, ${colorChange} 4s ease-in-out infinite;
`;