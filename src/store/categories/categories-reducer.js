import { actionTypes } from './categories-action-types'


const INITIAL_STATE = {
    categories: [],
    isLoading: false,
    error: null
}



const categoriesReducer = (
    state = INITIAL_STATE,
    action = {}
) => {
    const { type, payload } = action
    // console.log(`🚀 dispatching ${type}!`)

    switch (type) {
        case actionTypes.FETCH_CATEGORIES_START:
            return {
                ...state,
                isLoading: true,
                error: null
            }
        case actionTypes.FETCH_CATEGORIES_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload
            }
        case actionTypes.FETCH_CATEGORIES_SUCCESS:
            return {
                ...state,
                categories: payload
            }
        default:
            return state
    }
}

export {
    categoriesReducer
}