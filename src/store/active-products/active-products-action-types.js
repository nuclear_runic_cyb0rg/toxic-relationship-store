export const actionTypes = {
    CHANGE_ITEM_AMOUNT: "activeProducts/CHANGE_ITEM_AMOUNT",
    SET_ACTIVE_PRODUCT_LIST: "activeProducts/SET_ACTIVE_PRODUCT_LIST",
    SET_CART_OPEN: "activeProducts/SET_CART_OPEN"
}