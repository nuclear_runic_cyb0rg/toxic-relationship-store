import { Outlet } from "react-router-dom";
import {
    NavigationContainer,
    NavigationLogoContainer,
    NavigationLogoTitle,
    NavigationLinksContainer,
    NavigationLink,
    RootBackground,
    OutletContainer
} from './navigation.styles'

import { signOutUser } from "../../utils/firebase/firebase.utils";
import CartIcon from "../../components/header/cart-icon.component/cart-icon.component";
import { useSelector } from "react-redux";

const Navigation = () => {

    const currentUser = useSelector(state => state.user.currentUser)

    const signOutHandler = async () => {
        const res = await signOutUser()
        console.log(res)
    }

    return (
        <RootBackground>
            <NavigationContainer>
                <NavigationLogoContainer to="/">
                    <image src="../../assets/logo.svg" alt="Logo" />
                    <NavigationLogoTitle>
                        <b>🤍 Dive Into Toxic Relationship!</b>
                    </NavigationLogoTitle>
                </NavigationLogoContainer>
                <NavigationLinksContainer>
                    <NavigationLink to="info">
                        <b>INFO</b>
                    </NavigationLink>
                    <NavigationLink to="marketplace">
                        <b>MARKET</b>
                    </NavigationLink>
                    {!currentUser ?
                        (<NavigationLink to="sign-in">
                            <b>SIGN IN</b>
                        </NavigationLink>) :
                        (<NavigationLink as="span" onClick={signOutHandler}>
                            <b>SIGN OUT</b>
                        </NavigationLink>)
                    }
                    <CartIcon />
                </NavigationLinksContainer>
            </NavigationContainer>
            <OutletContainer>
                <Outlet />
            </OutletContainer>
        </RootBackground>
    )
}

export default Navigation