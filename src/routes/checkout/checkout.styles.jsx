import styled from "styled-components"

//* Exports:
// CheckoutContainer,
// CheckoutItem,
// CheckoutItemImage,
// CheckoutItemsList,
// CheckoutControlPanel,
// ChangeAmountButton,
// TotalPrice

export const CheckoutContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: space-between;
    gap: 10px;
    padding: 40px;
    margin: 10px 30%;
    border: 1px solid #ccc;
    border-radius: 10px;
    background: linear-gradient( to right, #2b0007, #000000);
    color: white
`

export const CheckoutItem = styled.div`
    display: grid;
    grid-template-columns: auto auto auto auto;
`

export const CheckoutItemImage = styled.img`
    border-radius: 5px;
    width: 60px;
    height: 60px;
`
export const CheckoutItemsList = styled.div`
        .payment-process-product-list-container {
            display: flex;
            flex-direction: column;
            align-items: baseline;
            gap: 10px;
            padding: 10px;
            /* background: linear-gradient(to right, #4caf50, #388e3c); */
        }
`

export const CheckoutControlPanel = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 2px;
    border-radius: 5px;
`

export const ChangeAmountButton = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    transition: background-color 0.3s;
    cursor: pointer;
    outline: none;
    border: none;
    border-radius: 50%;
    background-color: #4caf50;
    width: 30px;
    height: 30px;
    color: white;
    font-size: 18px;
    
    &:hover {
        background-color: #388e3c;
    }
    
    &:disabled {
        cursor: not-allowed;
        background-color: #ccc;
            }
`

export const TotalPrice = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 50px;
    padding: 0px 45px;
    font-weight: bold;
`