import SignUpForm from '../../components/authentication/sign-up-form.component/sign-up-form.component.jsx';
import SignIn from '../../components/authentication/sign-in-form.component/sign-in-form.component.jsx';
import { AuthenticationContainer } from './authentication.route.styles';

const SignInForm = () => {

    return (
        <AuthenticationContainer>
            <SignIn />
            <SignUpForm />
        </AuthenticationContainer>
    )
}

export default SignInForm