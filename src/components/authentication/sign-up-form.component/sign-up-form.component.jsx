import { createUserDocumentByEmailAndPassword, createUserDocumentByAuth } from "../../../utils/firebase/firebase.utils.js"
import { useState } from "react"
import {
    SignUpFormContainer,
    SignUpTitle,
    SignUpButtonContainer
}
from './sign-up-form.component.styles'
import FormInput from "../form-input-container.component/form-input-container-component.jsx"
import Button from "../../common/button.component/button.component.jsx"


const defaultSubmitForm = {
    displayName: '',
    email: "",
    password: "",
    confirmPassword: ""
}


const SignUpForm = () => {

    const [formData, setFormData] = useState(defaultSubmitForm)
    const { displayName, email, password, confirmPassword } = formData

    const resetSignUpForm = () => {
        setFormData(defaultSubmitForm)
    }

    const onChangeHandler = (event) => {
        const { name, value } = event.target
        setFormData({ ...formData, [name]: value })
    }

    const onSubmitHandler = async (event) => {
        event.preventDefault()


        if (password !== confirmPassword) {
            alert('Your passwords do not match :(')
            return
        }
        try {
            const { user } = await createUserDocumentByEmailAndPassword(email, password)
            // updateCurrentUser(user)
            createUserDocumentByAuth(user, { "displayName": displayName })
        }
        catch (error) {
            if (error.code === "auth/email-already-in-use") {
                alert('Your email is already in use!')
            }

            else if (error.code === "auth/invalid-email") {
                alert('Your email is invalid.')
            }
            else {
                console.log('We have issue: ', error)
            }


        }
        finally {
            resetSignUpForm()
        }
    }

    return (

        <SignUpFormContainer>
            <form onSubmit={onSubmitHandler}>
                <SignUpTitle>Don`t have an account?</SignUpTitle>
                <FormInput
                    label="Name"
                    type="text"
                    name="displayName"
                    value={displayName}
                    required
                    onChange={onChangeHandler}
                />
                <FormInput
                    label="Email"
                    type="email"
                    name="email"
                    value={email}
                    required
                    onChange={onChangeHandler} />
                <FormInput
                    label="Password"
                    type="password"
                    name="password"
                    value={password}
                    required
                    onChange={onChangeHandler} />
                <FormInput
                    label="Confirm password"
                    type="password"
                    name="confirmPassword"
                    value={confirmPassword}
                    required
                    onChange={onChangeHandler} />
                <SignUpButtonContainer>
                <Button text="Join us!" buttonType="default" type="submit"/>
                </SignUpButtonContainer>
            </form>
        </SignUpFormContainer>
    )
}


export default SignUpForm