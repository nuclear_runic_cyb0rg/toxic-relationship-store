import { actionTypes } from "./active-products-action-types"

const INITIAL_STATE = {
    activeProductList: [],
    isCartOpen: false
}

export const activeProductsReducer = (state = INITIAL_STATE, action) => {
    const { type, payload } = action
    switch (type) {
        case actionTypes.SET_ACTIVE_PRODUCT_LIST:
            return {
                ...state,
                activeProductList: payload
            }
        case actionTypes.CHANGE_ITEM_AMOUNT:
            return {
                ...state,
                activeProductList: payload
            }
        case actionTypes.SET_CART_OPEN:
            return {
                ...state,
                isCartOpen: payload
            }

        default:
            return state

    }
}