import styled from "styled-components";

export const AuthenticationContainer = styled.div`
    display: flex;
    justify-content: space-around;
    gap: 1em; 
    margin: 30px 390px;
    border-radius: 20px;
    background-position: center;
    background-size: cover;
    background-color: rgb(97, 0, 69);
    padding: 20px 30px;
    // grid-template-columns: auto auto;
    // column-gap: 10px;
    min-height: 30vh;
    color: #ffffff;
  
  /* Add additional styles for your content within .authentication-container if needed */
  &::before {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    opacity: 0.9; /* Adjust the opacity as desired */
    z-index: -1; /* Place the pseudo-element behind the content */
    background: url('../../assets/back-auth.jpg') repeat; /* Replace 'your_image_path.jpg' with the actual path to your image */
    padding-bottom: 200px;
    content: '';
  }

  /* Ensure child components take up equal width */
  & > * {
    flex: 1;
    max-width: 50%; /* Ensures each child takes up no more than 50% of the parent */
  }
  `