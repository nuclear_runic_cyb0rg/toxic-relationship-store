import { actionTypes } from './user-action-types'
const INITIAL_STATE = {
    currentUser: null
}

//* example of reducer
// reducer is a function that takes the current state
// and an action, and returns a new state
// it is used as a more complex version of setState
export const userReducer = (state = INITIAL_STATE, action) => {
    console.log(`🚀 dispatching ${action}!`)
    //? action always has:
    // type - string to identify the action
    // payload - object that gets passed to the reducer function

    // for instance: the action is ADD_INT and the payload is { value: 5 }
    const { type, payload } = action;

    switch (type) {
        case actionTypes.SET_CURRENT_USER:
            return {
                ...state,
                currentUser: payload
            }
        default:
            return state;
            // throw new Error(`Unhandled action type: ${type}`);
    }
}

