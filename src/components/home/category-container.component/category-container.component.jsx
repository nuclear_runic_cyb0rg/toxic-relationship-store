import {
    CategoryContainerStyle,
    BackgroundImage,
    CategoryBodyContainer,
    Title,
    Price
} from  "./category-container.styles"

const CategoryContainer = ({id, category}) => {
    const { title, imageURL } = category
    const imageStyle = {
        "backgroundImage": `url(${imageURL})`
      }
    return (
        <CategoryContainerStyle key={id} >
            <BackgroundImage style={imageStyle}></BackgroundImage>
            <CategoryBodyContainer>
              <Title>{title}</Title>
              <Price>Purchase now</Price>
            </CategoryBodyContainer>
        </CategoryContainerStyle>
    )
}

export default CategoryContainer;