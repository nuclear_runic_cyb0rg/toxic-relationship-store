import { CategoriesContainerStyle } from "./categories-container.styles"
import CategoryContainer from "../category-container.component/category-container.component";


const CategoriesContainer = ({categories}) => {
    return (
        <CategoriesContainerStyle>
    
          {categories.map(({ id, title, imageURL}) => {
              const category = {
                title: title,
                imageURL: imageURL
                }
              return <CategoryContainer id={id} category={category} />
          })}
        </CategoriesContainerStyle>
    )
}

export default CategoriesContainer