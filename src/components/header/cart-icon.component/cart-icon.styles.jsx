import styled from "styled-components";

//* exports:
// CartIconContainer,
// CartIconAmount

export const CartIconContainer = styled.span`
    margin: 0 5px 0 0;
    position: relative;
`

export const CartIconAmount = styled.span`
    position: absolute;
    color: rgb(61, 0, 54);
    z-index: 1001;
    top:50%;
    left: 50%;
    transform: translate(-50%, -50%);
`