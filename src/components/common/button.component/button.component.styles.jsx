import styled from "styled-components";

// * Exports:
//   BaseButton,
//   InversedButton,
//   GoogleButton

// Define your hazardous color palette inspired by Jojo's Bizarre Adventure
const $hazardous_pink = "#3e001c";
const $hazardous_light_pink=  "#2c0147"; // Adjusted shade
const $hazardous_yellow=  "#92eb0d";
const $hazardous_green= "#00431d";
const $hazardous_light_green=  "#87ffd5"; // Adjusted shade

export const BaseButton = styled.button`
  border: 2px solid #000;
  background-color: #3e001c;
  color: #fff;
  padding: 10px 20px;
  font-size: 16px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;
  font-weight: bold;
  // border-radius: 12px;

  &:hover {
    background-color: ${$hazardous_light_pink};
    color: ${$hazardous_light_green};
  }

  &:active {
    background-color: ${$hazardous_yellow};
    color: ${$hazardous_pink};
  }
`

export const InversedButton = styled(BaseButton)`
  background-color: #fff;
  color: #000;
  z-index: 1;
`

export const GoogleButton = styled(BaseButton)`
  background-color: ${$hazardous_green};
  color: #fff;
`