import { createSelector } from "reselect"


const selectCategoriesReducer = state => {
    // console.log('selector 1 pushed')
    return state.categories
}

export const categoriesListSelector = createSelector(
    [selectCategoriesReducer],
    categories => {
        // console.log('selector 2 pushed')
        return categories.categories
    }
)

//* reducer always returns a new object
export const selectCategories = createSelector(
    [categoriesListSelector],
    categories => {
        // console.log('selector 3 pushed')
        return categories.reduce((accumulator, category) => {
            const { title, items } = category
            accumulator[title.toLowerCase()] = items
            return accumulator
        }, {})
    }
)

export const selectCategoriesIsLoading = state => state.categories.isLoading