import CategoriesContainer from "../../components/home/categories-container.component/categories-container.component";

const categories = [
  {
    "id": 1,
    "title": "Pyjamas",
    "imageURL": "https://i.ibb.co/cvpntL1/hats.png"
  },
  {
    "id": 2,
    "title": "Costumes",
    "imageURL": "https://i.ibb.co/px2tCc3/jackets.png"
  },
  {
    "id": 3,
    "title": "Geeky",
    "imageURL": "https://i.ibb.co/0jqHpnp/sneakers.png"
  },
  {
    "id": 4,
    "title": "Criminal",
    "imageURL": "https://i.ibb.co/GCCdy8t/womens.png"
  },
  {
    "id": 5,
    "title": "Unconventional",
    "imageURL": "https://i.ibb.co/R70vBrQ/men.png"
  }
]

const Home = () => {
  return <CategoriesContainer categories= {categories}/>
};


export default Home