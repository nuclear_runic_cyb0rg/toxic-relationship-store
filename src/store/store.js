import { compose, createStore, applyMiddleware } from 'redux';
import { rootReducer } from './root-reducer';
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {thunk} from 'redux-thunk';

// CUSTOM MIDDLEWARE EXAMPLE
//! DO NOT USE IT
// eslint-disable-next-line no-unused-vars
const _loggerMiddleware = (store) => (next) => (action) => {
    if (!action.type) return next(action);

    console.log('type: ', action.type);
    console.log('payload: ', action.payload);
    console.log('state:', store.getState());

    next(action);

    console.log('next state:', store.getState());
}

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['user']
};

const persistedReducer = persistReducer(persistConfig, rootReducer)

const middlewares = [process.env.NODE_ENV !== 'production' && logger, thunk].filter(Boolean);

const composeEnhancer =  (process.env.NODE_ENV !== 'production' && window && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const enhancers = composeEnhancer(applyMiddleware(...middlewares));

export const store = createStore(persistedReducer, undefined, enhancers);
export const persistor = persistStore(store)
