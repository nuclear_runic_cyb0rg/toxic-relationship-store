import {
    ShopInfoContainer,
    ShopInfoTitle,
    ShopInfoParagraph
} from "./info.component.styles"

//TODO: Transfer infoData to firebase
//as the endpoint Stories or Blog or something
//=)

const infoData = [
    {
        title: "😊 Welcome to Toxic Relationship Store",
        paragraph: `
        Welcome to 'Toxic Relationship Store', where fashion meets
        the complexities of relationships. Embrace your
        unique journey with our exclusive collection
        designed for couples navigating the twists and
        turns of a toxic relationship. Whether it's
        sultry lingerie, cozy pajamas for those intense
        nights of discussion, playful costumes to add a
        spark, sophisticated suits for special occasions,
        or sportswear for those therapeutic workout
        sessions, our diverse range caters to every facet
        of your connection. At 'Toxic Relationship Store', we believe
        that every moment, even the challenging ones,
        deserves a touch of style and comfort.`},
    {
        title: "🎮 Toxic Relationship in Gaming",
        paragraph: `
        In the gaming world, toxic relationships extend beyond the screen.
        Whether it's in-game alliances turning sour or real-life
        connections strained by virtual conflicts, toxicity seeps into
        the gaming community. Competitive environments can escalate
        tensions, leading to verbal abuse, manipulation, and fractured
        friendships. Recognizing these toxic dynamics is crucial for
        preserving both your gaming experience and personal well-being.
        Establishing boundaries, promoting sportsmanship, and choosing
        positive gaming communities can help create a healthier gaming
        environment for all.`},
    {
        title: "💡 How to Understand Whether You Are in a Toxic Relationship",
        paragraph: `
        Identifying a toxic relationship is essential for personal growth
        and well-being. Signs include constant negativity, emotional
        manipulation, and a lack of mutual respect. Pay attention to
        how you feel in the relationship; if you often experience
        stress, anxiety, or sadness, it may be toxic. Observe
        communication patterns – constant criticism or control are red
        flags. Trust your instincts; if something doesn't feel right,
        it probably isn't. Seek support from friends or professionals
        to gain perspective. Remember, recognizing toxicity is the first
        step towards fostering healthier connections and building a
        more positive life.`},
]

const Shop = () => {
    return (
        <>
            {infoData.map((data, index) => (
                <ShopInfoContainer key={index}>
                    <ShopInfoTitle>{data.title}</ShopInfoTitle>
                    <ShopInfoParagraph>{data.paragraph}</ShopInfoParagraph>
                </ShopInfoContainer>
            ))}
        </>
    )
}

export default Shop