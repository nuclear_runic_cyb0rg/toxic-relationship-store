import { actionTypes } from "./active-products-action-types"
import { createAction } from "../../utils/reducer/create-action"
import { store } from "../store"

export const setActiveProductList = (activeProducts) => createAction(actionTypes.SET_ACTIVE_PRODUCT_LIST, activeProducts)

export const changeItemAmount = (item, operation) => {
    const { activeProductList } = store.getState().activeProducts

    let newAmount;

    switch (operation) {
        case "increase":
            newAmount = item.amount + 1;
            break;
        case "decrease":
            newAmount = item.amount - 1;
            break;
        case "delete":
            newAmount = 0;
            break;
        default:
            newAmount = item.amount;
            break;
    }

    const newProduct = {
        ...item,
        amount: newAmount
    }

    const newActiveProductList = activeProductList.map(product => {
        if (product.id === item.id) return newProduct
        else { return product }
    })
        .filter(i => i.amount > 0)

    return createAction(actionTypes.CHANGE_ITEM_AMOUNT, newActiveProductList)
}

export const setCartOpen = (bool) => {
    return createAction(actionTypes.SET_CART_OPEN, bool)
}