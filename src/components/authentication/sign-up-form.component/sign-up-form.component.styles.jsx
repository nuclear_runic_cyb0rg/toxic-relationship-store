import styled from 'styled-components';

//* exports:
// SignUpFormContainer,
// SignUpTitle,
// SignUpButtonContainer

export const SignUpFormContainer = styled.div`
  padding-left: 20px;
`
export const SignUpTitle = styled.h3`
  margin: 0 3px;
`
export const SignUpButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`