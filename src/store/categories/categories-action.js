import { actionTypes } from './categories-action-types';
import { createAction } from '../../utils/reducer/create-action';
import { getCollectionsAndProducts } from "../../utils/firebase/firebase.utils.js";

// const setCategories = (categories) => createAction(actionTypes.SET_CATEGORIES, categories);

export const fetchCategoriesStart = () => createAction(actionTypes.FETCH_CATEGORIES_START);
export const fetchSuccess = (categories) => createAction(actionTypes.FETCH_CATEGORIES_SUCCESS, categories);
export const fetchFailure = (error) => createAction(actionTypes.FETCH_CATEGORIES_FAILURE, error);

export const fetchCategoriesAsync = () => async (dispatch) => {
    dispatch(fetchCategoriesStart());
    try {
        const categoriesArray = await getCollectionsAndProducts();
        dispatch(fetchSuccess(categoriesArray));
    }
    catch (error) {
        dispatch(fetchFailure(error));
    }
}