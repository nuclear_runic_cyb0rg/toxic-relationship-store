// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {
  getAuth,
  signInWithRedirect,
  signInWithPopup,
  GoogleAuthProvider,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  onAuthStateChanged
} from "firebase/auth";

import {
  getFirestore,
  doc,
  getDoc,
  setDoc,
  collection,
  getDocs,
  query
} from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
  apiKey: "AIzaSyCXoAO3A5PaC5CqsgswU6t8l_17grl7nSo",
  authDomain: "toxic-relationship-store.firebaseapp.com",
  projectId: "toxic-relationship-store",
  storageBucket: "toxic-relationship-store.appspot.com",
  messagingSenderId: "453865198847",
  appId: "1:453865198847:web:bec3033cb17cd79385a8ca",
  measurementId: "G-P832L23YJ8",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const provider = new GoogleAuthProvider()

provider.setCustomParameters({
  prompt: 'select_account'
})

getAnalytics(app);



export const auth = getAuth();
export const signInWithGooglePopup = () => signInWithPopup(auth, provider)
export const signInWithGoogleRedirect = () => signInWithRedirect(auth, provider)
export const db = getFirestore()

export const createUserDocumentByAuth = async (userAuth, otherProps) => {
  const userDocRef = doc(db, 'users', userAuth.uid)
  // console.log(userAuth);
  // console.log(userDocRef);

  const userSnapshot = await getDoc(userDocRef)
  // console.log(userSnapshot);
  // console.log(userSnapshot.exists());

  const createdAt = Date()
  const { email } = userAuth

  if (!(userSnapshot.exists())) {
    try {
      await setDoc(userDocRef, {
        email,
        createdAt,
        ...otherProps
      })
    }
    catch (error) {
      console.log(error)
    }
  }

  return userDocRef
}

export const createUserDocumentByEmailAndPassword = async (email, password) => {
  const userCredential = await createUserWithEmailAndPassword(auth, email, password)
  return userCredential
}

export const checkUserEmail = async (email, password) => {
  try {
    const response = await signInWithEmailAndPassword(auth, email, password) 
    console.log(response)
    return response
    
  }
  catch (error) {
    console.log(error)
    console.log('================================')
    console.log(error.code)
    if (error.code === "auth/invalid-credential") {
      alert("Invalid credentials. Try again? =)")
    }
  }
}

export const logGoogleUserWithPopup = async () => {
    const { user } = await signInWithGooglePopup()
    createUserDocumentByAuth(user)
}

export const signOutUser = async () => {
  await signOut(auth)
}

export const onAuthStateChangedListener = async (callback) => {
  return onAuthStateChanged(auth, callback)
}

export const getCollectionsAndProducts = async() =>{
  const collectionName = 'categories'
  const collectionRef = collection(db, collectionName)
  const q = query(collectionRef)
  const querySnapshot = await getDocs(q)
  const categories = querySnapshot.docs
      .map(category => category.data())
  
  // .reduce((accumulator, docSnapshot) => {
  //   const { title, items } = docSnapshot.data()
  //   accumulator[title.toLowerCase()] = items
  //   return accumulator
  // }, {})
  return categories
}