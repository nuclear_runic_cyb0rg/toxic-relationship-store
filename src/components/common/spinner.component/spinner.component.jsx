import { SpinnerContainer, Spinner } from "./spinner.styles.jsx";

const SpinnerComponent = () => {
  return (<SpinnerContainer><Spinner /></SpinnerContainer>);
};

export default SpinnerComponent;