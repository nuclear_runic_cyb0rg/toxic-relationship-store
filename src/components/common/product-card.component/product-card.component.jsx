import  {InversedButton } from "../button.component/button.component.styles.jsx"
import {
    ProductCardContainer,
    ProductCardImage,
    ProductCardName,
    ProductCardPrice
} from "./product-card.styles.jsx"
import { useSelector, useDispatch } from "react-redux"
import { setActiveProductList } from "../../../store/active-products/active-products-action"
import { selectActiveProducts } from "../../../store/active-products/active-products-selector"
const ProductCard = ({ product }) => {

    const activeProductList = useSelector(selectActiveProducts)
    const dispatch = useDispatch()
    const addToCartHandler = () => {
        // console.log("ACTIVE PRODUCT LIST:\n", activeProductList)
        // console.log(product)

        if (!product.amount) product.amount = 1
        let newList = [...activeProductList]

        const existingIndex = newList.findIndex(item => item.id === product.id)
        // console.log(newList[existingIndex])
        // console.log(existingIndex)

        if (existingIndex !== -1) newList[existingIndex].amount += 1
        else newList.push(product)

        dispatch(setActiveProductList(newList))
        // console.log(newList)
    }


    return (

        <ProductCardContainer>
            <ProductCardImage src={product.imageUrl} alt={product.name} />
            <ProductCardName>{product.name}</ProductCardName>
            <ProductCardPrice>{"$" + product.price}</ProductCardPrice>
            <InversedButton onClick={addToCartHandler}>Add to Cart</InversedButton> 
        </ProductCardContainer>
    )
}

export default ProductCard