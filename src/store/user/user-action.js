import { actionTypes } from './user-action-types';
import { createAction } from '../../utils/reducer/create-action';

export const setCurrentUser = (user) => createAction(actionTypes.SET_CURRENT_USER, user);