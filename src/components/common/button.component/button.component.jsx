import { BaseButton, 
    InversedButton, 
    GoogleButton } from "./button.component.styles";

const BUTTON_TYPE_CLASSES = {
    base: 'default',
    inversed: 'inversed',
    google: 'google'
}

const getButton = (buttonType = BUTTON_TYPE_CLASSES.base) =>
({
    [BUTTON_TYPE_CLASSES.base]: BaseButton,
    [BUTTON_TYPE_CLASSES.inversed]: InversedButton,
    [BUTTON_TYPE_CLASSES.google]: GoogleButton
}[buttonType]);

const Button = ({ children, text, buttonType, ...otherProps }) => {
    const ButtonComponent = getButton(buttonType);
    if (text) {
        return <ButtonComponent {...otherProps} >{text}</ButtonComponent>
    }
    return <ButtonComponent {...otherProps}>{children}</ButtonComponent>
};

export default Button;