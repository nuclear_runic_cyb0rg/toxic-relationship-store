import { Route, Routes } from "react-router-dom"

import "./App.scss";

import {
  onAuthStateChangedListener,
  createUserDocumentByAuth
} from "./utils/firebase/firebase.utils.js";
import { useDispatch } from "react-redux";
import Navigation from "./routes/navigation/navigation.component.jsx";
import Home from './routes/home/home.component.jsx'
import Marketplace from "./routes/marketplace/marketplace.route.jsx"
import SignIn from "./routes/authentication/authentication.route.jsx";
import Info from "./routes/info/info.component.jsx"
import Checkout from './routes/checkout/checkout.component.jsx'
import { useEffect } from "react";
import { setCurrentUser } from "./store/user/user-action.js";

const App = () => {

  const dispatch = useDispatch()
  useEffect(() => {

    const syncFunction = () => {
      const unsubscribe = onAuthStateChangedListener((user) => {
        if (user) {
          createUserDocumentByAuth(user)
        }
        dispatch(setCurrentUser(user))
      })
      return unsubscribe
    }
    syncFunction()
  }, [dispatch])

  return (

    <Routes>
      {/* < GlobalButtonStyle /> */}
      <Route path="/" element={<Navigation />}>
        <Route index element={<Home />}></Route>
        <Route path="info" element={<Info />}></Route>
        <Route path='marketplace/*' element={<Marketplace />}></Route>
        <Route path='sign-in' element={<SignIn />}></Route>
        <Route path='process-payment' element={<Checkout />}></Route>
      </Route>
    </Routes>
  )
}

export default App;
