import styled from "styled-components";

const borderColor = "#ff90da";
const mainColor = "#440323";
// const nameColor = "#ff90da";
const priceColor = "#fff";

//* Exports:
// CartItemContainer,
// CartItemDeleteButton,
// CartItemTextContainer,
// CartItemImage,
// CartItemName,
// CartItemPrice

export const CartItemContainer = styled.div`
    // display!
    display: flex;
    position: relative;
    // flex-direction: column;
    gap: 15px;
    transition: all 0.3s ease;
    border: 2px solid ${borderColor};
    border-radius: 5px;
    background-color: ${mainColor};
    padding: 10px;
`

export const CartItemDeleteButton = styled.button`
        position: absolute;
        top: 0;
        right: 0;
        background-color: transparent;
        color: #ff90da;
        border: none;
        cursor: pointer;
        transition: background-color 0.3s, color 0.3s;
    
    &:hover {
        background-color: rgba(0, 0, 0, 0.2);
        color: #fff;
    }
    
    &:active {
        background-color: rgba(0, 0, 0, 0.4);
        color: #fff;
    }
`   

export const CartItemTextContainer  = styled.span`
        display: flex;
        flex-direction: column;
`

export const CartItemImage = styled.img`
        width: 50px;
        height: 50px;
`

export const CartItemName = styled.span`
        margin-bottom: 5px;
        color: rgb(132, 252, 252);
        font-weight: bold;
        font-size: 13px;
`

export const CartItemPrice = styled.span`
    color: ${priceColor};
    font-size: 16px;


    &:hover {
        transform: scale(0.95);
        border-width: 4px;
    }
`