import styled from 'styled-components';
//* exports:
// ProductCardContainer
// ProductCardImage,
// ProductCardName,
// ProductCardPrice,

import {
  BaseButton,
  InversedButton,
  GoogleButton
} from "../button.component/button.component.styles"



export const ProductCardName = styled.span`
  width: auto;
  margin-bottom: 15px;
  position: absolute;
  background-color: rgb(0, 0, 0);
  color: white;
  z-index: 1;
  display: relative;
  bottom: 5px;
  text-align: center;
  padding: 2px;
  `

export const ProductCardPrice = styled.span`
  width: auto;
  display: relative;
  justify-content: space-between;
  font-size: 20px;
  color: white;
  background-color: black;
  padding: 0px 5px;
  position: absolute;
  bottom: 45px;
  z-index: 1;
`

export const ProductCardImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  z-index: 1;
  border-radius: 15px;
`


export const ProductCardContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 300px;
  align-items: center;
  position: relative;
  border-radius: 15px;
  
  background-color: rgb(9, 8, 8);
  border: 1px solid rgb(8, 5, 5);
  box-shadow: 0px 0px 10px rgb(0, 0, 0);
  z-index: 0;
  text-overflow: wrap;
  overflow: hidden;
  transition: all 1s linear;

  ${BaseButton},
  ${InversedButton},
  ${GoogleButton} {
    position: absolute;
    top: 50%;
    display: none;
    opacity: 0.85;
}

    &:hover {
      ${ProductCardImage} {
        opacity: 0.8;
        width: 105%;
        height: 105%;
        transition: all 1s linear;
      }
      ${ProductCardName},
      ${ProductCardPrice}{
        display: none;
      }
      ${InversedButton}       {
        display: inherit;
        z-index: 1;
      }
    }
`
