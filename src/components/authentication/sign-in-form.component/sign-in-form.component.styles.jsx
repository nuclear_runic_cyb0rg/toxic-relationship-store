import styled from 'styled-components';

//* exports: 
// LoginForm, 
// ButtonContainer

export const LoginForm = styled.form`
    max-width: 600px;
    border-radius: 10px;
    flex: 1
`

export const ButtonContainer = styled.div`
    display: grid;
    grid-template-columns: auto auto;
`