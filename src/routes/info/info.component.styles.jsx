import styled from "styled-components";

//* Exports:
// ShopInfoContainer,
// ShopInfoTitle,
// ShopInfoParagraph

export const ShopInfoContainer = styled.div`
    background-color: rgba(248, 248, 248, 0.8); // Adjust opacity as needed
    border: 2px solid #ddd;
    border-radius: 8px;
    padding: 0px 20px;
    margin: 20px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);

    &:hover {
        background-color: #eaeaea;
    }

    &::before {
        background: linear-gradient(to bottom, #6f0030, transparent);
        z-index: -1;
        width: 100%;
        height: 100%;
            
        }
`

export const ShopInfoTitle = styled.h2`
    color: #6f0030;
    font-size: 1.5em;
    margin-bottom: 10px;
`

export const ShopInfoParagraph = styled.p`
    color: #000000;
    font-size: 1em;
    line-height: 1.4;
`
