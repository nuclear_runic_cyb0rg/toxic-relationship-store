import styled from "styled-components";
import { 
    BaseButton,
    InversedButton,
    GoogleButton
 } from "../../common/button.component/button.component.styles"

//* exports:
// CartDropdownContainer,
// CartDropdownSum,
// ProceedToCheckoutContainer,
// CartItemsContainer,
// CartItems,
// AddToCartButtonContainer

export const CartDropdownContainer = styled.div`
  /* Positioning */
  position: absolute;
  top: 38px; 
  right: 55px; 
  justify-content: center; /* Center content horizontally */

  
  flex-direction: column;
  overflow: hidden;
  /* Z-index to control stacking order */
  z-index: 999; 
  
  /* Background and text color */
  background-color: rgb(97, 0, 69); 
  color: #fff; 
  
  /* Border styles */
  border: 2px solid #fff;
  border-radius: 8px;
  
  /* Box shadow for visual effect */
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
  
  /* Spacing and padding */
  padding: 11px;
  min-height: 100px;
  max-height: 500px;
  min-width: 300px;

  ${BaseButton},
  ${InversedButton},
  ${GoogleButton} {
    margin-top: 10px;
  }
`

export const CartDropdownSum = styled.div`
    display: flex;
    justify-content: center;
    font-size: 20px;
    font-weight: bold;
    padding: 7px 0px ;
`

export const ProceedToCheckoutContainer = styled.div`
    display: flex; /* Use flexbox */
    justify-content: center; /* Center content horizontally */
    align-items: center;
`

export const CartItemsContainer = styled.div`
  max-height: 300px;
  overflow-y: auto;
  overflow-x: hidden;
`

export const CartItems = styled.div`
  min-height: 130px;
  padding: 10px;
`

export const AddToCartButtonContainer = styled.div`
  display: flex; /* Use flexbox */
  justify-content: center; /* Center content horizontally */
  align-items: center;
`
export const NoItemText = styled.div`
    display: flex;
    justify-content: center;
    font-size: 20px;
    font-weight: bold;
    padding: 7px 0px ;
    color: rgb(255, 228, 247);
`