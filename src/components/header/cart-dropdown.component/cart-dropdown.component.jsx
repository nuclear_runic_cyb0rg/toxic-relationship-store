import Button from "../../common/button.component/button.component"
import CartItem from "../cart-item.component/cart-item.component"
import { useNavigate } from "react-router-dom"
import { selectActiveProducts } from "../../../store/active-products/active-products-selector"
import { useSelector, useDispatch } from "react-redux"
import { setCartOpen } from "../../../store/active-products/active-products-action"

import {
    CartDropdownContainer,
    CartDropdownSum,
    ProceedToCheckoutContainer,
    CartItemsContainer,
    NoItemText
} from "./cart-dropdown.component.styles"

const CartDropdown = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const activeProductList = useSelector(selectActiveProducts)

    const _countSum = () => {

        let sum = 0
        for (let product of activeProductList) {
            let productTotal = product.price * product.amount
            sum += productTotal
        }
        return sum
    }


    const proceedToCheckoutHandler = () => {
        dispatch(setCartOpen(false))
        navigate("/process-payment")
    }
    return (
        <CartDropdownContainer>
            <CartItemsContainer>
                {

                    activeProductList.length > 0 ? activeProductList.map(item => {
                        return <CartItem key={item.id} product={item} />
                    }) :

                        (
                            <NoItemText>
                                No items in your cart
                            </NoItemText>
                        )
                }
            </CartItemsContainer>
            <CartDropdownSum>
                Total: {_countSum()} $
            </CartDropdownSum>
            <ProceedToCheckoutContainer>
                <Button onClick={proceedToCheckoutHandler} text="proceed" buttonType="inversed" />
            </ProceedToCheckoutContainer>
        </CartDropdownContainer>)
}

export default CartDropdown