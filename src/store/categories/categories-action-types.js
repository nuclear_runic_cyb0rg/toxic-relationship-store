export const actionTypes = {
    FETCH_CATEGORIES_START: 'categories/FETCH_START',
    FETCH_CATEGORIES_SUCCESS: 'categories/FETCH_SUCCESS',
    FETCH_CATEGORIES_FAILURE: 'categories/FETCH_FAIL'
}