
import { selectCategories, selectCategoriesIsLoading } from "../../../store/categories/categories-selector.js";
import ProductCard from "../../common/product-card.component/product-card.component";
import SpinnerComponent from "../../common/spinner.component/spinner.component.jsx";
import {
  MarketplaceContainer,
  CategoryContainer,
  CategoryTitle,
  CategoryProductsContainer
} from "./marketplace-preview.styles.jsx";

import { useSelector } from "react-redux";

const MarketplacePreview = () => {

  const categories = useSelector(selectCategories);
  const isLoading = useSelector(selectCategoriesIsLoading);
  // if (!categories) {
  //   return <div>Loading...</div>;
  //   }
  const keys = Object.keys(categories);
  // console.log(categories)
  // console.log(keys)

  return (
    <MarketplaceContainer>
      {isLoading ? keys.map((key) => (
        <CategoryContainer key={key}>
          <CategoryTitle to={`/marketplace/${key}`}>{key}</CategoryTitle>
          <CategoryProductsContainer>
            {categories[key].slice(0, 4).map((product) => (
              <ProductCard key={product.id} product={product} />
            ))}

          </CategoryProductsContainer>
        </CategoryContainer>
      )) : <SpinnerComponent />}
    </MarketplaceContainer>
  );
};


export default MarketplacePreview;
